{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0035f481",
   "metadata": {},
   "source": [
    "# LEGO colour mapper for RGB images\n",
    "\n",
    "*Ben Loveday ([@brloveday](https://twitter.com/brloveday)) and Hayley Evers-King ([@HayleyEversKing](https://twitter.com/hayleyeversking)), June 2021*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99bb3297",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "### Introduction\n",
    "\n",
    "As avid Lego collectors and geospatial scientists, we could not resist the latest [Lego Art World Map](https://www.lego.com/en-gb/product/world-map-31203). However, as we are never one to use things 'out of the box', we decided to write a little Python tool to map any RGB image or any data to the supplied 'pixels'. This notebook can be used to will map RGB data using the closest available colours. It's data mapper counterpart ([LEGO colour mapper for data](LEGO_colour_mapper_for_data.ipynb)) will map data using the colours as a rough Blue -> Green -> Red colour palette, corresponding to increasing magnitude.\n",
    "\n",
    "The code is offered without warranty...e.g. we accept no responsibility if there are errors in the tile maps, which you discover on day 3 of your build!\n",
    "\n",
    "We hope you enjoy using this, and please let us know if you do! Lego is life!\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d82d1da1",
   "metadata": {},
   "source": [
    "### Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11630785",
   "metadata": {},
   "source": [
    "Firstly, we must import our required libraries. You can use the environment.yml file to make sure you have everything you need if you use conda."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8cc381e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import configparser\n",
    "import matplotlib.pyplot as plt\n",
    "from skimage.transform import resize\n",
    "import numpy as np\n",
    "import os\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "\n",
    "# specific functions\n",
    "import support_functions as sf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0322fb0",
   "metadata": {},
   "source": [
    "Now we select our image. Our example uses an RGB image of global chlorophyll, derived from the CMEMS OC-CCI CHL concentration. The image is trimmed so that there are no borders / axes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4afa2c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "input_file = os.path.join(os.getcwd(),\"Test_data\",\"CMEMS_OC-CCI_global_CHLA_climatology.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13d31fc6",
   "metadata": {},
   "source": [
    "Most of the Lego kit settings, and some user options are stored in the config.ini file. These setting are loaded in below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42cfcc5d",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = configparser.ConfigParser()\n",
    "config.read('config.ini')\n",
    "print('Read in config')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b38a86ca",
   "metadata": {},
   "source": [
    "Now we have read in the configuration options, we build the Lego grid. The mask tiles are stored in the *./Mask* directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "573d8581",
   "metadata": {},
   "outputs": [],
   "source": [
    "# set LEGO grid\n",
    "x_grid, y_grid, land_mask = sf.get_grid_mask(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05e4a3e2",
   "metadata": {},
   "source": [
    "Next we read in the supplied image and remove the alpha channel if there is one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c1a4971",
   "metadata": {},
   "outputs": [],
   "source": [
    "im = plt.imread(input_file)\n",
    "# remove alpha channel\n",
    "im = im[:,:,:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78c36993",
   "metadata": {},
   "source": [
    "The next box resamples the image on the LEGO grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0f5ec94d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# note order of coords depends on image dims\n",
    "image_resized = resize(im, (y_grid, x_grid), anti_aliasing=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13a0660a",
   "metadata": {},
   "source": [
    "We now find the closest matches between the RGB palette used for the image and the colours provided by Lego. Unlike in the data case, there is no masking here. Everything is done with raw colour matching."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "add2c72c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# find anomalies between image colours and avaliable LEGO colours\n",
    "anomalies = np.zeros((len(config['COLOURS']), y_grid, x_grid))\n",
    "RGB_cols = []\n",
    "for ncol, col_item, dot_item in zip(range(len(config['COLOURS'])), config['COLOURS'], config['NDOTS']):\n",
    "    RGB_col = np.array([int(config['COLOURS'][col_item][1:][i:i+2], 16) for i in (0, 2, 4)]).astype(float)/255\n",
    "    RGB_cols.append(RGB_col)\n",
    "    anomalies[ncol,:,:] = ((image_resized[:,:,0] - RGB_col[0])**2 \\\n",
    "                      + (image_resized[:,:,1] - RGB_col[1])**2 \\\n",
    "                      + (image_resized[:,:,2] - RGB_col[2])**2)**0.5\n",
    "\n",
    "selection_rank = np.argsort(anomalies, axis=0)\n",
    "sorted_anomalies = np.take_along_axis(anomalies, selection_rank, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae6c479f",
   "metadata": {},
   "source": [
    "Now we map the RGB palette to the Lego colours; initially without asusming our Lego pixels are limited."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad7d9f93",
   "metadata": {},
   "outputs": [],
   "source": [
    "im_out = image_resized.copy()*np.nan\n",
    "print('No limit mapping....')\n",
    "for ii in np.arange(x_grid):\n",
    "    for jj in np.arange(y_grid):\n",
    "        im_out[jj,ii,:] = RGB_cols[selection_rank[0,jj,ii]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f67d0da",
   "metadata": {},
   "source": [
    "...and check what colours were used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c5d66cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# check what colours were used vs what we have\n",
    "pts_used = 0\n",
    "for ncol, col, dots in zip(range(len(config['COLOURS'])), config['COLOURS'], config['NDOTS']):\n",
    "    pts_used = pts_used + np.sum(np.sum(selection_rank[0,:,:] == ncol))\n",
    "    print('Colour: {0}, dots available {1}, dots used {2}'.format(col,config['NDOTS'][dots],\\\n",
    "                                                           np.sum(np.sum(selection_rank[0,:,:] == ncol))))\n",
    "print('----')\n",
    "print('Total {0} of {1}'.format(str(pts_used), str(128*80)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb3fa201",
   "metadata": {},
   "source": [
    "In the above, you can usually see that 'optimal' RGB matching wants more of a certain colour than you have. So lets re-run our mapping, imposing a limit on the pixels available based on what we have."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7aa3917f",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# map the image to LEGO colours with a resource limit for one set\n",
    "print('Resource limited mapping....')\n",
    "im_out_lim = image_resized.copy()\n",
    "\n",
    "pts_used = np.zeros(len(config['COLOURS']))\n",
    "pts_available = []\n",
    "for item in config['NDOTS']:\n",
    "    pts_available.append(int(config['NDOTS'][item]))\n",
    "\n",
    "mask = np.zeros(np.shape(sorted_anomalies[0,:,:]))\n",
    "final_plan = np.zeros(np.shape(mask))\n",
    "for rank in range(np.shape(selection_rank)[0]):\n",
    "    for col in range(len(config['COLOURS'])):\n",
    "        ii,jj = np.where((selection_rank[rank,:,:] == col) & (mask == 0))\n",
    "        sort_indices = np.argsort(sorted_anomalies[0,ii,jj].ravel())\n",
    "        for si in sort_indices:\n",
    "            if pts_used[col] < pts_available[col]:\n",
    "                mask[ii[si],jj[si]] = 1\n",
    "                im_out_lim[ii[si],jj[si],:] = RGB_cols[col]\n",
    "                final_plan[ii[si],jj[si]] = col\n",
    "                pts_used[col] = pts_used[col] + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60b6b359",
   "metadata": {},
   "source": [
    "...and again check what we use in the 'limited' case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa194c5b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# check what colours were used vs what we have\n",
    "for ncol, col, dots in zip(range(len(config['COLOURS'])), config['COLOURS'], config['NDOTS']):\n",
    "    print('Colour: {0}, dots available {1}, dots used {2}'.format(col,config['NDOTS'][dots],pts_used[ncol]))\n",
    "print('----')\n",
    "print('Total {0} of {1}'.format(str(int(sum(pts_used))),str(128*80)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f36b75a4",
   "metadata": {},
   "source": [
    "Now lets look at how our colour mapping is looking."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72dc97d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(4, 1, figsize=(10,20))\n",
    "axs[0].imshow(im)\n",
    "axs[0].set_title('Original image')\n",
    "axs[1].imshow(image_resized)\n",
    "axs[1].set_title('Re-sampled image')\n",
    "axs[2].imshow(im_out)\n",
    "axs[2].set_title('Colour mapped image without resource limits')\n",
    "axs[3].imshow(im_out_lim)\n",
    "axs[3].set_title('Colour mapped image with resource limits')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3e4470a",
   "metadata": {},
   "source": [
    "And in the final step we write out our plan to a series of tiles, in the same way as the original Lego instructions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2d2773f",
   "metadata": {},
   "outputs": [],
   "source": [
    "sf.write_out_plan(config, final_plan, RGB_cols)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
