# Lego_satellite_maps

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/middle_earth_observation%2Flego_satellite_maps.git/main)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://gitlab.com/middle_earth_observation/lego_satellite_maps.git/main)

A public repository of tools and Jupyter Notebooks for mapping satellite data and/or images to the Lego Art World map. These notebooks were built for python 3 using the Anaconda Individual edition for MacOSx, available here https://www.anaconda.com/products/individual.

# Prerequities

* Required: A python environment that support Jupyter Notebook (The installation instructions below are for Anaconda, which is recommended)
* Optional: a command line that supports *git* (Note: the Anaconda Prompt satisfies this requirement, as do Linux/OSx terminals)

# Installation and configuration instructions

The Lego_satellite_maps tool kit can be installed and run as follows:

1. **Sourcing the tools:**
    
   If you have access to a git compatible command line, you can add the tools using: \
   > **git clone https://gitlab.com/middle_earth_observation/lego_satellite_maps.git**
   
   *(the Anaconda prompt will support this command)*

   If you do not have command line access, you can download and unzip the tools from: \
   **https://gitlab.com/middle_earth_observation/lego_satellite_maps** \
   *(the download button is the little down arrow next to 'clone')*

2. **Setting up the python environment:**

   If you are comfortable with using the command line, you can navigate to the downloaded package source directory and configure and activate the enviroment as follows:

   > cd ./lego_satellite_maps \
   > conda env create -f environment.yml \
   > conda activate lego_maps

   If you are using Anaconda Navigator you should import the environment.yml file by following these instructions: https://docs.anaconda.com/anaconda/navigator/tutorials/manage-environments/

3. **Launching Jupyter**

   From the command line, you can launch Jupyter Notebooks by typing *jupyter-notebook* or *jupyter notebook* (depending on your OS). Note that you need to ensure that your terminal has the correct python environment activate

   Alternatively, you can run Jupyter Notebook from inside Anaconda Navigator, making sure that you have the correct environment selected.

4. **Now your good to go; open either of the notebooks included here, and follow the instructions within!**

