{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c73ca5d3",
   "metadata": {},
   "source": [
    "# LEGO colour mapper for data\n",
    "\n",
    "*Ben Loveday ([@brloveday](https://twitter.com/brloveday)) and Hayley Evers-King ([@HayleyEversKing](https://twitter.com/hayleyeversking)), June 2021*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bf17422",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "### Introduction\n",
    "\n",
    "As avid Lego collectors and geospatial scientists, we could not resist the latest [Lego Art World Map](https://www.lego.com/en-gb/product/world-map-31203). However, as we are never one to use things 'out of the box', we decided to write a little Python tool to map any RGB image or any data to the supplied 'pixels'. This notebook can be used to map and netCDF data to the map, provided it is regularly gridded. The colours are used as a rough Blue -> Green -> Red colour palette, corresponding to increasing magnitude. It's image mapper counterpart ([LEGO colour mapper for images](LEGO_colour_mapper_for_images.ipynb)) will map RGB images using the closest available colours.\n",
    "\n",
    "The code is offered without warranty...e.g. we accept no responsibility if there are errors in the tile maps, which you discover on day 3 of your build!\n",
    "\n",
    "We hope you enjoy using this, and please let us know if you do! Lego is life!\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eabbdd27",
   "metadata": {},
   "source": [
    "### Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "569ce5d7",
   "metadata": {},
   "source": [
    "Firstly, we must import our required libraries. You can use the environment.yml file to make sure you have everything you need if you use conda."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac445451",
   "metadata": {},
   "outputs": [],
   "source": [
    "import configparser\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy import stats\n",
    "import numpy as np\n",
    "import netCDF4 as nc\n",
    "import os\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "\n",
    "# specific functions\n",
    "import support_functions as sf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9127cbf9",
   "metadata": {},
   "source": [
    "Now we select our data. We need to supply the file name and variables of interest, including the coordinate variables. Our demo plot uses a global climatology of Chlorophyll concentration, derived from the European Commission's [Copernicus Marine Environment Monitoring Service (CMEMS) catalogue](https://resources.marine.copernicus.eu/?option=com_csw&view=details&product_id=OCEANCOLOUR_GLO_CHL_L4_REP_OBSERVATIONS_009_093)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7b032078",
   "metadata": {},
   "outputs": [],
   "source": [
    "# input file\n",
    "input_file = os.path.join(os.getcwd(),\"Test_data\",\"CMEMS_OC-CCI_global_CHLA_climatology.nc\")\n",
    "\n",
    "# variables (and units)\n",
    "nc_variable = \"CHL\"\n",
    "nc_latitude = \"latitude\"\n",
    "nc_longitude = \"longitude\"\n",
    "nc_units=\"$mg.m^{-3}$\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0442da38",
   "metadata": {},
   "source": [
    "Most of the Lego kit settings, and some user options are stored in the config.ini file. The settings include the colours of the dots and the number of each available (we are just using the dots that come with the world map kit here). These settings are loaded in below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e37b9794",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = configparser.ConfigParser()\n",
    "config.read('config.ini')\n",
    "print('Read in config.ini')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65aacffc",
   "metadata": {},
   "source": [
    "Now we have read in the configuration options, we build the Lego grid. The mask tiles are stored in the *./Mask* directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3245e3d",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "x_grid, y_grid, land_mask = sf.get_grid_mask(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eef06bfc",
   "metadata": {},
   "source": [
    "Next we read in the supplied data and take the log value if specified in the config.ini"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fce7d0c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "nc_fid = nc.Dataset(input_file)\n",
    "im = np.squeeze(nc_fid.variables[nc_variable][:])\n",
    "if config['OPTS']['log_data'] == '1':\n",
    "    im[im < 0] = np.nan\n",
    "    im = np.log10(im)\n",
    "lon = nc_fid.variables[nc_longitude][:]\n",
    "lat = nc_fid.variables[nc_latitude][:]\n",
    "nc_fid.close()\n",
    "LON, LAT = np.meshgrid(lon, lat)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97e31f13",
   "metadata": {},
   "source": [
    "The next box resamples the data on the LEGO grid. We assume that our data is higher resultion that our Lego map, so this resampling is done through binning, rather than interpolation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6dbedf75",
   "metadata": {},
   "outputs": [],
   "source": [
    "image_resized = stats.binned_statistic_2d(LAT.ravel(), LON.ravel(), im.ravel(), np.nanmean,\\\n",
    "                                          bins=[np.linspace(-90,90,y_grid+1),\\\n",
    "                                          np.linspace(-180,180,x_grid+1)])\n",
    "image_resized = image_resized[0]\n",
    "if config['OPTS']['image_flip_ud'] == '1':\n",
    "    image_resized = image_resized[::-1,:]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04f87b20",
   "metadata": {},
   "source": [
    "The LEGO mask is not quite 'geolocated' with the data mask, so we move data slightly so that they correspond. You can control this with the 'lon_pix_shift' paramter in config.ini, and may need to adapt it for your data. We also 'mop up' any 'NaN' points that may not fall under the Lego mask, but replacing them with the minimum value in the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5864add",
   "metadata": {},
   "outputs": [],
   "source": [
    "# mask_shift\n",
    "image_resized = np.concatenate((image_resized[:,int(config['OPTS']['lon_pix_shift']):],\\\n",
    "                                image_resized[:,:int(config['OPTS']['lon_pix_shift'])]), axis=1)\n",
    "\n",
    "# handle the mask and 'fill' where required\n",
    "image_resized[np.isnan(image_resized)] = np.nanmin(image_resized)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "332e8ef8",
   "metadata": {},
   "source": [
    "Now we read in the colours and their associated 'resource' of pixels. Lego actually supply more pixels that will fit in the map. Excepting the mask pixels, we chop the resource number down for each colour so that it fits the map but retains the best colour spread."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c4e6857",
   "metadata": {},
   "outputs": [],
   "source": [
    "ndots = [0]\n",
    "RGB_cols = []\n",
    "cols = []\n",
    "for ncol, col_item, dot_item in zip(range(len(config['COLOURS'])), config['COLOURS'], config['NDOTS']):\n",
    "    RGB_col = np.array([int(config['COLOURS'][col_item][1:][i:i+2], 16) for i in (0, 2, 4)]).astype(float)/255\n",
    "    # scale down large dot pools to fit image, keeping the best spread of colours\n",
    "    these_dots = int(config['NDOTS'][dot_item])\n",
    "    if these_dots > 1500 and these_dots < 2000:\n",
    "        these_dots = these_dots - 300\n",
    "    elif these_dots > 1000 and these_dots < 1500:\n",
    "        these_dots = these_dots - 288\n",
    "    ndots.append(these_dots)\n",
    "    RGB_cols.append(RGB_col)\n",
    "    cols.append(ncol)\n",
    "dot_vals = np.cumsum(ndots)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8158a7f",
   "metadata": {},
   "source": [
    "Now we map the colours to the data on a point by point basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "972c58d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "im_out = image_resized.copy()\n",
    "im_out[land_mask == 0] = -9999\n",
    "im_out[land_mask == 1] = -999\n",
    "\n",
    "image_1D = im_out.ravel()\n",
    "indices = np.argsort(image_1D)\n",
    "colbar_vals = []\n",
    "for ii in range(1,len(dot_vals)):\n",
    "    min_val = min(image_1D[indices[dot_vals[ii-1]:dot_vals[ii]]])\n",
    "    max_val = max(image_1D[indices[dot_vals[ii-1]:dot_vals[ii]]])\n",
    "    if min_val == max_val == -9999: colbar_vals.append('Land')\n",
    "    elif min_val == max_val == -999: colbar_vals.append('Shadow')\n",
    "    else:\n",
    "        if config['OPTS']['log_data'] == '1':\n",
    "            min_val = 10**min_val\n",
    "            max_val = 10**max_val\n",
    "        colbar_vals.append(\"{0:.2g} - {1:.2g} {2}\".format(min_val, max_val, nc_units))\n",
    "    image_1D[indices[dot_vals[ii-1]:dot_vals[ii]]] = cols[ii-1]\n",
    "\n",
    "im_out = image_1D.reshape(np.shape(image_resized))\n",
    "im_out_lim = np.zeros((np.shape(image_resized)[0],np.shape(image_resized)[1],3))\n",
    "for ii in np.arange(x_grid):\n",
    "    for jj in np.arange(y_grid):\n",
    "        im_out_lim[jj,ii,:] = RGB_cols[int(im_out[jj,ii])]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0387d963",
   "metadata": {},
   "source": [
    "Let's now do a quick check to make sure our resource usage in within what we actually have available (in terms of number of dots of each colour). Because we have a limited and non-equal number of each colour, the colour map is not linear (it's also not colour blind friendly or perceptually uniform etc, you'll have to wait until we can buy more dots of other colours for that!) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6c724a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# check what colours were used vs what we have\n",
    "for ncol, col, dots in zip(range(len(config['COLOURS'])), config['COLOURS'], config['NDOTS']):\n",
    "    print('Colour: {0}, dots available {1}, dots used {2}'.format(col,config['NDOTS'][dots],sum(sum(im_out == ncol))))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5704467b",
   "metadata": {},
   "source": [
    "In the penultimate step, we make some plots to compare our original image, our resampled image and our mapped colour palette image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6348051",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(3, 1, figsize=(10,15))\n",
    "axs[0].imshow(im)\n",
    "axs[0].set_title('Original image')\n",
    "axs[1].imshow(image_resized)\n",
    "axs[1].set_title('Re-sampled image')\n",
    "axs[2].imshow(im_out_lim)\n",
    "axs[2].set_title('Colour mapped image with resource limits')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf1c6a2b",
   "metadata": {},
   "source": [
    "And in the final step we write out our plan to a series of tiles, in the same way as the original Lego instructions. We also create a colour bar so you can see what the colours map to in terms of values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a45b8592",
   "metadata": {},
   "outputs": [],
   "source": [
    "sf.write_out_plan(config, im_out, RGB_cols, colbar_vals=colbar_vals)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
