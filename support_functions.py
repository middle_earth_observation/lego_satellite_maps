import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def get_grid_mask(config):
    '''
     Calculates the grid and mask
     
     inputs:
     ------
     config:         configuration file dictionary
     
     outputs:
     ------
     x_grid:         total number of x points
     y_grid:         total number of y points
     land_mask:      the 2 dimensional land mask
    '''
    
    # set grid variables
    x_grid = int(config['TILES']['tile_size_x']) * int(config['TILES']['number_of_tiles_x'])
    y_grid = int(config['TILES']['tile_size_y']) * int(config['TILES']['number_of_tiles_y'])

    # build the land mask
    land_mask = np.zeros((y_grid, x_grid))
    land = 0
    shadow = 0
    
    # loop through the mask tiles
    x1 = 0
    for xx in range(int(config['TILES']['number_of_tiles_x'])):
        y1 = 0
        for yy in range(int(config['TILES']['number_of_tiles_y'])):
            this_tile = os.path.join(os.getcwd(),'Mask',\
                    'Land_mask_x{0}_y{1}.csv'.format(str(xx).zfill(2),str(yy).zfill(2)))
            try:
                vals = np.genfromtxt(this_tile, delimiter=',', dtype='int')
                land_mask[y1:y1+16,x1:x1+16] = vals
                land = land + sum(sum(vals == 1))
                shadow = shadow + sum(sum(vals == 2))
            except:
                pass
            y1 = y1 + 16
        x1 = x1 + 16
    
    # offset the mask
    land_mask = land_mask -1
    
    # report mask values
    print('Land points: {0}'.format(str(land)))
    print('Shadow points: {0}'.format(str(shadow)))
   
    return x_grid, y_grid, land_mask

# ---

def write_out_plan(config, output_image, RGB_cols, colbar_vals=None):
    '''
     Writes out the plan as a series of png images tiles.
     
     inputs:
     ------
     config:         configuration file dictionary
     output_image:   the initial output image to be tiled
     RGB_cols:       the RGB colour list
     
     outputs:
     ------
     None
    '''
    
    # set grid variables
    x_grid = int(config['TILES']['tile_size_x']) * int(config['TILES']['number_of_tiles_x'])
    y_grid = int(config['TILES']['tile_size_y']) * int(config['TILES']['number_of_tiles_y'])
    grid = np.arange(int(config['TILES']['tile_size_x']))

    # set colour map variables for discrete mapping
    cmap = mpl.colors.ListedColormap(RGB_cols)
    cmap.set_over('0.25')
    cmap.set_under('0.5')
    bounds=[-0.5,0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    # make colorbar
    if colbar_vals:
        fig = plt.figure(figsize=(5, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_facecolor('k')
        plt.scatter(np.ones(len(colbar_vals)), np.arange(len(colbar_vals)), \
                    s=600, c=np.arange(len(colbar_vals)), cmap=cmap, norm=norm)
        for ii in np.arange(len(colbar_vals)):
            plt.text(3,ii-0.125,colbar_vals[ii],c='w',fontsize=14)
        # remove axes
        plt.text(-1,len(colbar_vals)*0.3,'Colour values', rotation=90, c='w', fontsize=20)
        ax.axes.xaxis.set_visible(False)
        ax.axes.yaxis.set_visible(False)
        plt.xlim([-2, 10])
        plt.savefig('Colourbar.png')
        plt.close(fig)
    
    x_count = -1
    for xx in np.linspace(0, x_grid, int(x_grid/int(config['TILES']['tile_size_x'])+1))[0:-1]:
        x_count = x_count + 1
        y_count = -1
        for yy in np.linspace(0, y_grid, int(y_grid/int(config['TILES']['tile_size_y'])+1))[0:-1]:
            y_count = y_count + 1

            # determine this tile
            this_tile = output_image[int(yy):int(yy+int(config['TILES']['tile_size_y'])),\
                                     int(xx):int(xx+int(config['TILES']['tile_size_x']))]
            
            # flip ud (images read top to bottom)
            this_tile = this_tile[::-1,:]
            
            # set up plot
            fig = plt.figure(figsize=(8, 8))
            ax = fig.add_subplot(1, 1, 1)
            ax.set_facecolor('k')

            # make the plot
            plt.scatter(np.tile(grid[None,:],16), np.tile(grid[:, None],16), s=600, c=this_tile,\
                        cmap=cmap, norm=norm)
            
            # write numbers
            for ii in grid:
                for jj in grid:
                    if this_tile[jj,ii] == 1 or this_tile[jj,ii] == 3 or this_tile[jj,ii] == 8:
                        col = 'w'
                    else:
                        col = 'k'
                    if this_tile[jj,ii] == 9:
                        toff = 0.32
                    else:
                        toff = 0.16
                    plt.text(ii-toff,jj-0.2,str(int(this_tile[jj,ii])+1),c=col,fontsize=14)
                    
            # remove axes
            ax.axes.xaxis.set_visible(False)
            ax.axes.yaxis.set_visible(False)
            
            # set output name
            output_file = config['OPTS']['image_prefix'] + \
              '_x{0}_y{1}.png'.format(str(x_count).zfill(2), str(y_count).zfill(2))
            
            # write output
            plt.savefig(output_file, bbox_inches='tight')
            plt.close(fig)